package com.example.roomautomigration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.roomautomigration.data.entity.ConversationModel
import com.example.roomautomigration.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var conversationListAdapter: ConversationListAdapter
    private val conversationViewModel: ConversationViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpConversationAdapter()
        setUpObservers()
        generateData()
    }

    private fun setUpConversationAdapter() {
        val layoutManager = LinearLayoutManager(this)
        conversationListAdapter = ConversationListAdapter()
        binding.rvConversationList.layoutManager = layoutManager
        binding.rvConversationList.adapter = conversationListAdapter
    }

    private fun setUpObservers() {
        conversationViewModel.getConversationList().observe(this, { conversationList ->
            conversationListAdapter.update(conversationList)
            Log.d("ConversationList", "-$conversationList")
        })
    }

    private fun generateData() {
        val conversationList: ArrayList<ConversationModel> = arrayListOf()
        conversationList.add(ConversationModel("1", "Kruno", "Android"))
        conversationList.add(ConversationModel("2", "Nikola", "VP"))
        conversationList.add(ConversationModel("3", "Martin", "Android"))
        conversationList.add(ConversationModel("4", "Ivan", "IOS"))
        conversationList.add(ConversationModel("5", "Tomislav", "Android"))
        conversationList.add(ConversationModel("6", "Marty", "Lead"))
        conversationViewModel.insertConversationList(conversationList)
    }
}