package com.example.roomautomigration.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.roomautomigration.data.entity.ConversationModel

@Dao
interface ConversationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(conversations: List<ConversationModel>)

    @Query("SELECT * FROM conversation ORDER BY id ASC")
    fun conversations(): LiveData<List<ConversationModel>>
}