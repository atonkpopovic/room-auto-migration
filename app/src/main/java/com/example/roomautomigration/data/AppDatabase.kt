package com.example.roomautomigration.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.roomautomigration.data.dao.ConversationDao
import com.example.roomautomigration.data.entity.ConversationModel

@Database(
    entities = [ConversationModel::class],
    version = 1,
    exportSchema = true
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun conversationDao(): ConversationDao
}