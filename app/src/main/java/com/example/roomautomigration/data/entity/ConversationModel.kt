package com.example.roomautomigration.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "conversation")
data class ConversationModel(
    @PrimaryKey @field:SerializedName("id") var id: String,
    @field:SerializedName("title") var title: String? = "",
    @field:SerializedName("subtitle") var subtitle: String? = "",
)
