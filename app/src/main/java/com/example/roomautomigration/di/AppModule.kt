package com.example.roomautomigration.di

import android.content.Context
import androidx.room.Room
import com.example.roomautomigration.data.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "test-db"
        ).build()
    }

    @Singleton
    @Provides
    fun provideConversationDao(db: AppDatabase) = db.conversationDao()
}