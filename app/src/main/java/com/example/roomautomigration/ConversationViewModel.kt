package com.example.roomautomigration

import androidx.lifecycle.ViewModel
import com.example.roomautomigration.data.dao.ConversationDao
import com.example.roomautomigration.data.entity.ConversationModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ConversationViewModel @Inject constructor(private val conversationDao: ConversationDao) :
    ViewModel() {

    fun getConversationList() = conversationDao.conversations()

    fun insertConversationList(conversationList: ArrayList<ConversationModel>) {
        CoroutineScope(Dispatchers.IO).launch {
            conversationDao.insert(conversationList)
        }
    }
}