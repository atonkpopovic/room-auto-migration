package com.example.roomautomigration

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.roomautomigration.data.entity.ConversationModel
import com.example.roomautomigration.databinding.ConversationItemBinding

class ConversationListAdapter : RecyclerView.Adapter<ConversationListAdapter.ViewHolder>() {

    private lateinit var binding: ConversationItemBinding
    private var conversationList: ArrayList<ConversationModel> = arrayListOf()

    fun update(data: List<ConversationModel>) {
        conversationList.clear()
        conversationList.addAll(data)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ConversationItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ConversationModel) {
            binding.tvId.text = data.id
            binding.tvTitle.text = data.title
            binding.tvSubtitle.text = data.subtitle
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding =
            ConversationItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (conversationList.isNullOrEmpty()) return
        holder.bind(conversationList[position])
    }

    override fun getItemCount(): Int {
        return conversationList.size
    }
}